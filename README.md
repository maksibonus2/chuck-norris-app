# Chuck Norris Web application

This project created like front-end test task.

## Getting Started

### Prerequisites

To use current project you will need to install:

1. NPM.

### Installation

1. Run command in terminal in the folder where located package.json:
    ```
    npm install
    ```

2. Run command in terminal:
    ```
    npm run start
    ```
   
### Running the tests

For running tests you need to run command:
    ```
    npm run test
    ```

## Deployment

For creating build for deployment you need to run command:
    ```
    npm run build
    ```

After that will be created build for deployment in the folder "dist".

## Authors

* **Maksym Syzrantsev** - *Initial work*

## Additional

For more information you can write me to [email](maksibonus@gmail.com).

Developed by Maksym Syzrantsev as front-end task.
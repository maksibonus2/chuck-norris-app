import communicate from "../util/communicate/communicate";

export const getJokeRequest = () => communicate({
  url: "jokes/random",
  method: "GET"
});

export const getJokeByCategoryRequest = (category) => communicate({
  url: `jokes/random?category=${category}`,
  method: "GET"
});
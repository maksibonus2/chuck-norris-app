import * as actions from '../actions/categoryActions';
import * as requests from './getCategoryRequest';

function fetchCategories() {
  return (dispatch) => {
    const result = requests.getCategoriesRequest();
    result.then((res) => {
      dispatch(actions.addCategories(res.data));
    });
  }
}

export default fetchCategories;
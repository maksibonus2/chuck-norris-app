import * as requests from './getJokeRequest';

export function fetchJoke() {
  return requests.getJokeRequest().then((res) => res.data);
}

export function fetchCategoryJoke(category) {
  return requests.getJokeByCategoryRequest(category).then((res) => res.data);
}
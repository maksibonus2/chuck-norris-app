// @flow
/* eslint-disable */

import communicate from "../util/communicate/communicate";

export const getCategoriesRequest = () => communicate({
  url: "jokes/categories",
  method: "GET"
});
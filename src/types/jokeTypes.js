export type Joke = {
  id: string,
  categories: Array<string>,
  created_at: string,
  updated_at: string,
  url: string,
  value: string,
  icon_url: string,
  isFavourite: boolean,
  toggleJokeFavourite: (id: string) => void
}

// @flow
// eslint-disable react/prefer-stateless-function

import React from 'react';
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import JokeItemList from "../Jokes/JokeItemList";
import type { Joke } from "../../types/jokeTypes";
import AddJokeButton from "../Jokes/AddJokeButton";
import { removeJoke } from "../../actions/jokeActions";
import { removeDisabledCategory } from "../../actions/categoryActions";
import { useMediaQuery } from "react-responsive";

const useStyles = () => ({
  jokesPanel: {
    width: "100%",
    height: "75vh",
    display: "block"
  },
  jokeListDesktop: {
    width: "auto",
    display: "block",
    maxHeight: "90%",
    overflow: "auto"
  },
  jokeListMobile: {
    width: "auto",
    display: "block",
    maxHeight: "80%",
    overflow: "auto"
  }
});

type Props = {
  // eslint-disable-next-line react/require-default-props
  classes?: Object,
  jokes: Array<Joke>,
  disabledCategories: Array<string>,
  currentCategory: string,
  tabNumber: number,
  removeDisabledCategory: (category: string) => void,
  removeJoke: (id: string) => void
}

const GenericPanel = (props: Props) => {
  const {
    classes, jokes, currentCategory, tabNumber, disabledCategories
  } = props;

  const isMobile = useMediaQuery({ query: '(max-width: 600px)' });
  const tryRemoveJoke = (id) => {
    if (disabledCategories.includes(currentCategory)) {
      props.removeDisabledCategory(currentCategory);
    }
    props.removeJoke(id);
  }

  return (
    <Grid
      container
      direction="column"
      justify="flex-start"
      alignItems="flex-start"
      className={classes.jokesPanel}
    >
      { tabNumber === 0 && (<AddJokeButton
        disabledCategories={disabledCategories}
        currentCategory={currentCategory}
        jokes={jokes}
      />)}

      <Grid item className={isMobile ? classes.jokeListMobile : classes.jokeListDesktop}>
        <JokeItemList
          jokes={jokes}
          currentCategory={currentCategory}
          removeJoke={tryRemoveJoke}
        />
      </Grid>
    </Grid>
  );
}

const mapStateToProps = (state) => (
  {
    disabledCategories: state.categoryReducer.disabledCategories
  });

const mapDispatchToProps = (dispatch) => (
  {
    removeDisabledCategory: (category) => dispatch(removeDisabledCategory(category)),
    removeJoke: (id) => dispatch(removeJoke(id))
  });

export default withStyles(useStyles)(connect(mapStateToProps, mapDispatchToProps)(GenericPanel));
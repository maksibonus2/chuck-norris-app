// @flow
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import Box from '@material-ui/core/Box';

export type Props = {
  // eslint-disable-next-line react/require-default-props
  children?: React.ReactNode,
  index: any,
  value: any
};

function TabPanel(props: Props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

export default TabPanel;
// @flow
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Tab, ButtonBase } from "@material-ui/core";

function NumerableTab(props: any) {
  const { counter, ...rest } = props;

  return (
    <Tab
      {...rest}
      component={React.forwardRef((inheritedProps, ref) => {
        const { children, ...restProps } = inheritedProps;
        return (
          <ButtonBase ref={ref} {...restProps}>
            {children}
            <p>{counter}</p>
          </ButtonBase>);
      })}
    />);
}

export default NumerableTab;
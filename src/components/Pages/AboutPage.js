// @flow
/* eslint-disable jsx-a11y/img-redundant-alt */

import React from "react";
import Responsive from 'react-responsive';
import { makeStyles } from "@material-ui/core/styles";
import Dog from '../../images/dog.jpg'
import useWindowDimensions from '../../util/ScreenSizes';

const useStyles = makeStyles(() => ({
  img: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

export default function AboutPage() {
  const classes = useStyles();

  const { width } = useWindowDimensions();

  return (
    <div>
      <p>
        Hi! I am Maksym Syzrantsev. I am .NET developer.
      </p>
      <p>
        I like programming, bike ride, play in computer games, snowboarding, spend time with friends.
      </p>
      <p>
        When I feels sad, I look for funny pictures or pictures with cute animals. For example: like this little puppy.
      </p>
      <div className={classes.img}>
        <Responsive query="(max-width:600px)">
          <img src={Dog} alt="Picture of cute dog" width={width} height={width / 2} />
        </Responsive>
        <Responsive query="(min-width:601px)">
          <img src={Dog} alt="Picture of cute dog" />
        </Responsive>
      </div>
    </div>);
}
// @flow
// eslint-disable react/prefer-stateless-function
// eslint-disable no-await-in-loop

import React, {
  useCallback, useEffect, useMemo, useState
} from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Responsive, { useMediaQuery } from "react-responsive";
import { withStyles } from '@material-ui/core/styles';
import MoodIcon from '@material-ui/icons/Mood';
import MenuIcon from "@material-ui/icons/Menu";
import FavoriteIcon from '@material-ui/icons/Favorite';
import {
  Tab, Tabs, Paper, Grid, Drawer, IconButton, Typography
} from "@material-ui/core";
import type { Joke } from "../../types/jokeTypes";
import GenericPanel from "../Panels/GenericPanel";
import fetchCategoriesAction from '../../api/fetchCategory';
import CategoryItemList from "../Categories/CategoryItemList";


const useStyles = (theme) => ({
  root: {
    flexGrow: 1
  },
  wrapper: {
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'stretch'
  },

  mainContainer: {
    flexWrap: "nowrap",
    height: "auto",
    maxHeight: "75vh"
  },

  genericPanel: {
    width: "100%",
    height: "75vh",
    display: "block"
  },

  hide: {
    display: "none"
  },

  desktopTab: theme.secondaryTabDesktop,
  mobileTab: theme.secondaryTabMobile
});

type Props = {
  // eslint-disable-next-line react/require-default-props
  classes?: Object,
  categories: Array<string>,
  currentCategory: string,
  jokes: Array<Joke>,
  isInitialized: boolean,
  fetchCategories: () => void
}

const JokesPage = (props: Props) => {
  const {
    classes, categories, currentCategory, jokes, isInitialized, fetchCategories
  } = props;

  const isMobile = useMediaQuery({ query: '(max-width: 600px)' });
  const [panelName, setPanelName] = useState("jokes");
  const [openDrawer, setOpenDrawer] = useState(false);

  useEffect(() => {
    if (!isInitialized) {
      fetchCategories();
    }
  });

  const setActiveTab = (event, newValue) => {
    switch (newValue) {
      case 0:
        setPanelName("jokes");
        break;
      case 1:
        setPanelName("favorites");
        break;
      default:
        setPanelName("jokes");
        break;
    }
  };

  const showingJokes = useMemo(() => {
    switch (panelName) {
      case "jokes":
        return jokes;
      case "favorites":
        return jokes.filter((item: Joke) => item.isFavourite === true);
      default:
        return [];
    }
  }, [panelName, jokes]);

  const getTabNumber = useMemo(() => (panelName === "jokes" ? 0 : 1), [panelName]);

  const calculateCounts = useCallback((category) => {
    if (category === "all") {
      return showingJokes.length;
    } else {
      return showingJokes.filter((item: Joke) => item.categories.includes(category)).length;
    }
  }, [showingJokes]);

  const toggleDrawer = () => {
    setOpenDrawer(!openDrawer);
  };

  return (
    <Paper square className={classes.root}>
      <Responsive query="(max-width:600px)">
        <Tabs
          className={classes.wrapper}
          value={getTabNumber}
          onChange={setActiveTab}
          centered
        >
          <Tab icon={<MoodIcon />} label="General" className={classes.mobileTab} />
          <Tab icon={<FavoriteIcon />} label="Favorites" className={classes.mobileTab} />
        </Tabs>
      </Responsive>
      <Responsive query="(min-width:601px)">
        <Tabs
          className={classes.wrapper}
          value={getTabNumber}
          onChange={setActiveTab}
          centered
        >
          <Tab icon={<MoodIcon />} label="General" className={classes.desktopTab} />
          <Tab icon={<FavoriteIcon />} label="Favorites" className={classes.desktopTab} />
        </Tabs>
      </Responsive>

      <Grid
        container
        direction={isMobile ? "column" : "row" }
        className={classes.mainContainer}>
        <Grid item>
          <Responsive query="(max-width:600px)">
            <IconButton
              aria-label="open drawer"
              onClick={toggleDrawer}
              edge="start"
            >
              <MenuIcon />
              <Typography variant="h6" noWrap>
                Categories
              </Typography>
            </IconButton>
            <Drawer anchor="left" open={openDrawer} onClose={toggleDrawer}>
              <CategoryItemList
                panelName={panelName}
                currentCategory={currentCategory}
                categories={categories}
                calculateCounts={calculateCounts}
              />
            </Drawer>
          </Responsive>
          <Responsive query="(min-width:601px)">
            <CategoryItemList
              panelName={panelName}
              currentCategory={currentCategory}
              categories={categories}
              calculateCounts={calculateCounts}
            />
          </Responsive>
        </Grid>
        <Grid item>
          <GenericPanel
            currentCategory={currentCategory}
            tabNumber={getTabNumber}
            jokes={showingJokes}
          />
        </Grid>
      </Grid>
    </Paper>
  );
}

const mapStateToProps = (state) => (
  {
    isInitialized: state.categoryReducer.isInitialized,
    categories: state.categoryReducer.allCategories,
    jokes: state.jokeReducer.jokes,
    currentCategory: state.categoryReducer.currentCategory
  });

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchCategories: fetchCategoriesAction
}, dispatch);

export default withStyles(useStyles)(connect(mapStateToProps, mapDispatchToProps)(JokesPage));
// @flow
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Responsive from 'react-responsive';
import {
  makeStyles, Tabs, Tab, AppBar
} from "@material-ui/core";
import JokesPage from "./JokesPage";
import AboutPage from "./AboutPage";

const useStyles = makeStyles((theme) => ({
  invisibleTab: {
    display: "none"
  },
  desktopAppBar: theme.desktopAppBar,
  mobileAppBar: theme.mobileAppBar,
  mobileTabs: theme.mobileTabs,
  desktopTab: theme.desktopTab,
  mobileTab: theme.mobileTab
}));

export default function MainPage() {
  const classes = useStyles();

  const allTabs = ["/jokes", "/about"];
  const nameTabs = ["Jokes", "About"];

  return (
    <Router>
      <div>
        <Route
          path="/"
          render={({ location }) => (
            <>
              <Responsive query="(max-width:600px)">
                <AppBar position="static" classes={{ root: classes.mobileAppBar }}>
                  <Tabs centered value={allTabs.includes(location.pathname) ? location.pathname : false} className={classes.mobileTabs}>
                    {allTabs.map((item, index) => (<Tab label={nameTabs[index]} value={item} component={Link} to={item} classes={{ root: classes.mobileTab }} key={nameTabs[index]} />))}
                  </Tabs>
                </AppBar>
              </Responsive>

              <Responsive query="(min-width:601px)">
                <AppBar position="static" classes={{ root: classes.desktopAppBar }}>
                  <Tabs value={allTabs.includes(location.pathname) ? location.pathname : false}>
                    {allTabs.map((item, index) => (<Tab label={nameTabs[index]} value={item} component={Link} to={item} classes={{ root: classes.desktopTab }} key={nameTabs[index]} />))}
                  </Tabs>
                </AppBar>
              </Responsive>

              <Switch>
                <Route path={allTabs[0]} render={() => <JokesPage />} />
                <Route path={allTabs[1]} render={() => <AboutPage />} />
                <Route exact path="*">
                  <Redirect to={allTabs[0]} />
                </Route>
              </Switch>
            </>
          )}
        />
      </div>
    </Router>
  );
}
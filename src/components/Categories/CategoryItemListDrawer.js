// @flow
// eslint-disable react/prefer-stateless-function

import React from 'react';
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Tabs from "@material-ui/core/Tabs";
import TabPanel from "../Tabs/TabPanel";
import NumerableTab from "../Tabs/NumerableTab";
import { changeCategory } from "../../actions/categoryActions";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: "65vh"
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    display: 'flex',
    flexDirection: 'column'
  }
}));

function CategoryItemList(props) {
  const {
    categories, calculateCounts, currentCategory, ...rest
  } = props;

  const classes = useStyles();
  const [value, setValue] = React.useState(categories.indexOf(currentCategory));

  const handleChange = (event, newValue) => {
    const nextCategoryName = categories[newValue];
    rest.changeCategory(nextCategoryName);
    setValue(newValue);
  };

  const tabItems = categories.map((item, index) => <NumerableTab counter={calculateCounts(item)} label={item} key={item} index={index} />);

  const tabPanels = categories.map((item, index) => (<TabPanel value={value} index={index} key={item} />));

  return (
    <div className={classes.root}>
      <Tabs
        variant="scrollable"
        scrollButtons="auto"
        orientation="vertical"
        value={value}
        onChange={handleChange}
        className={classes.tabs}
      >
        {tabItems}
      </Tabs>
      {tabPanels}
    </div>
  );
}

const mapStateToProps = (state) => (
  {
    jokes: state.jokeReducer.jokes
  });

const mapDispatchToProps = (dispatch) => (
  {
    changeCategory: (category) => dispatch(changeCategory(category))
  });

export default connect(mapStateToProps, mapDispatchToProps)(CategoryItemList);
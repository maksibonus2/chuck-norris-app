// @flow
// eslint-disable camelcase

import moment from "moment";
import React, { useState, createRef } from 'react';
import { connect } from 'react-redux';
import {
  Avatar, IconButton, ListItem, ListItemText, ListItemIcon, ListItemAvatar, TextField
} from "@material-ui/core";
import { useMediaQuery } from "react-responsive";
import { withStyles } from "@material-ui/core/styles";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';
import { toggleJokeFavourite } from "../../actions/jokeActions";

const useStyles = () => ({
  desktopRoot: {
    display: "flex",
    alignItems: "flex-start"
  },
  mobileRoot: {
    display: "block",
    alignItems: "flex-start"
  },
  avatar: {
    alignItems: "top",
    display: "inline-block"
  },
  buttons: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  icons: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  largeIcons: {
    marginLeft: "5px",
    marginRight: "5px",
    width: "60px",
    height: "60px"
  }
});

const JokeItem = (props) => {
  const {
    // eslint-disable-next-line camelcase
    classes, id, value, created_at, icon_url, isFavourite, editJoke, removeJoke, toggleFavourite
  } = props;

  const isMobile = useMediaQuery({ query: '(max-width: 600px)' });

  const [editMode, setEditMode] = useState("false");
  const [errorText, setErrorText] = useState("");

  let inputRef = createRef();

  const saveClick = () => {
    if (inputRef.value === "") {
      setErrorText("Incorrect joke");
      return;
    }

    editJoke(id, inputRef.value, moment().format('YYYY-MM-DD HH:MM:ss.SSSSSS'));
    toggleEditMode();
  }

  const toggleEditMode = () => {
    const currentEditMode = editMode;
    setEditMode(!currentEditMode);
    setErrorText("");
  }

  const addEditButtons = () => {
    if (editMode === true) {
      return (
        <ListItemIcon className={classes.icons}>
          <SaveIcon onClick={saveClick} className={isMobile ? classes.largeIcons : null} />
          <CancelIcon onClick={() => toggleEditMode()} className={isMobile ? classes.largeIcons : null} />
        </ListItemIcon>
      );
    } else {
      return (
        <ListItemIcon className={classes.icons}>
          <EditIcon onClick={() => toggleEditMode()} className={isMobile ? classes.largeIcons : null} />
          <DeleteIcon onClick={() => removeJoke(id)} className={isMobile ? classes.largeIcons : null} />
        </ListItemIcon>
      );
    }
  }

  const applyEditMode = () => {
    if (editMode === true) {
      const error = errorText === "" ? null : true;

      return (
        <TextField
          id="outlined-basic"
          label="Edit joke"
          variant="outlined"
          style={{ margin: 8 }}
          fullWidth
          InputLabelProps={{
            shrink: true
          }}
          error={error}
          helperText={errorText}
          defaultValue={value}
          inputRef={(input) => {
            inputRef = input;
          }}
        />
      );
    } else {
      return (
        // eslint-disable-next-line camelcase
        <ListItemText primary={value} secondary={created_at} />
      );
    }
  }

  return (
    <ListItem direction="column" className={isMobile ? classes.mobileRoot : classes.desktopRoot}>
      <ListItemAvatar className={classes.avatar}>
        <Avatar>
          {/* eslint-disable-next-line camelcase */}
          <img src={icon_url} alt="" />
        </Avatar>
      </ListItemAvatar>
      {applyEditMode()}
      <div className={classes.buttons}>
        <ListItemIcon className={classes.largeIcons}>
          <IconButton onClick={() => toggleFavourite(id)} className={isMobile ? classes.largeIcons : null}>
            {isFavourite ? <StarIcon className={isMobile ? classes.largeIcons : null} /> : <StarBorderIcon className={isMobile ? classes.largeIcons : null} /> }
          </IconButton>
        </ListItemIcon>
        {addEditButtons()}
      </div>

    </ListItem>);
}

const mapDispatchToProps = (dispatch) => (
  {
    toggleFavourite: (id) => dispatch(toggleJokeFavourite(id))
  });

export default withStyles(useStyles)(connect(null, mapDispatchToProps)(JokeItem));

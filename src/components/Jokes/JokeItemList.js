// @flow
// eslint-disable react/jsx-props-no-spreading
import React from 'react';
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import type { Joke } from "../../types/jokeTypes";
import JokeItem from "./JokeItem";
import { editJoke } from "../../actions/jokeActions";

const useStyles = () => ({
  root: {
    width: '100%',
    display: 'block'
  }
})

type Props = {
  // eslint-disable-next-line react/require-default-props
  classes?: Object,
  currentCategory: string,
  jokes: Array<Joke>,
  editJoke: (id: string, text: string) => void,
  removeJoke: (id: string) => void
}

class JokeItemList extends React.Component<Props> {
  getFilteredJokes() {
    if (this.props.currentCategory === "all") {
      return this.props.jokes;
    } else {
      return this.props.jokes.filter((item: Joke) => item.categories.includes(this.props.currentCategory));
    }
  }

  renderRows() {
    return this.getFilteredJokes().map((joke: Joke) => (
      <JokeItem
        {...joke}
        key={joke.id}
        editJoke={this.props.editJoke}
        removeJoke={this.props.removeJoke}
      />
    ));
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        {this.renderRows()}
      </div>)
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    // eslint-disable-next-line camelcase
    editJoke: (id, text, updated_at) => dispatch(editJoke(id, text, updated_at))
  });

export default withStyles(useStyles)(connect(null, mapDispatchToProps)(JokeItemList));
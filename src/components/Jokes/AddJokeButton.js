// @flow

import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  Grid,
  Button,
  Snackbar,
  withStyles
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import type { Joke } from "../../types/jokeTypes";
import { addJoke } from "../../actions/jokeActions";
import { addDisabledCategory } from "../../actions/categoryActions";

import { fetchCategoryJoke, fetchJoke } from "../../api/fetchJoke";

const useStyles = (theme) => ({
  button: theme.button
});

type Props = {
  // eslint-disable-next-line react/require-default-props
  classes? : Object,
  jokes: Array<Joke>,
  currentCategory: string,
  disabledCategories: Array<string>,
  addJoke: (joke: Joke) => void,
  addDisabledCategory: (category: string) => void
}

type State = {
  openNoJokeLeftMessage: boolean
};

const initialState = {

  openNoJokeLeftMessage: false
};

class AddJokeButton extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  async handleAddJoke() {
    if (this.props.disabledCategories.includes(this.props.currentCategory)) {
      this.setState({ openNoJokeLeftMessage: true });
    } else {
      await this.tryGetJokeAsync();
    }
  }

  tryGetJokeAsync = async () => {
    let isJokeExists = false;
    let wasAdded = false;
    let joke;
    let repeatCount = 4;

    while (repeatCount > 0) {
      // eslint-disable-next-line no-await-in-loop
      joke = await this.getJokeAsync();

      // add only unique jokes
      // eslint-disable-next-line no-loop-func
      isJokeExists = this.props.jokes.some((item) => (item.id === joke.id));

      if (isJokeExists === false) {
        this.props.addJoke(joke);
        wasAdded = true;
        break;
      }
      repeatCount -= 1;
    }

    if (!wasAdded && !this.props.disabledCategories.includes(this.props.currentCategory)) {
      this.props.addDisabledCategory(this.props.currentCategory);
      this.setState({ openNoJokeLeftMessage: true });
    }
  }

  getJokeAsync = async () => {
    let joke;

    if (this.props.currentCategory === "all") {
      joke = await fetchJoke();
    } else {
      joke = await fetchCategoryJoke(this.props.currentCategory);
    }

    return joke;
  }

  closeMessage() {
    this.setState({ openNoJokeLeftMessage: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Grid item>
          <Button className={classes.button} variant="contained" color="primary" onClick={async () => this.handleAddJoke()}>
            Get joke
          </Button>
        </Grid>
        <Snackbar open={this.state.openNoJokeLeftMessage} autoHideDuration={2000} onClose={() => this.closeMessage()}>
          <Alert onClose={() => this.closeMessage()} severity="warning">
            There is no more joke for current category
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => (
  {
    disabledCategories: state.categoryReducer.disabledCategories
  });

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addDisabledCategory: (category) => addDisabledCategory(category),
  addJoke: (joke) => addJoke(joke)
}, dispatch);

export default withStyles(useStyles)(connect(mapStateToProps, mapDispatchToProps)(AddJokeButton));
// @flow
// eslint-disable react/prefer-stateless-function
import { hot } from 'react-hot-loader/root';
import React from 'react';
import MainPage from "../Pages/MainPage";

class App extends React.Component<any> {
  componentDidCatch(error: Error, errorInfo: any) {
    console.error(error, errorInfo);
  }

  render() {
    return (
      <div>
        <MainPage />
      </div>
    );
  }
}

export default hot(App);
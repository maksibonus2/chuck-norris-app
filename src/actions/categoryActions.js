/* eslint-disable */
export const ADD_CATEGORIES = 'GET_CATEGORIES_REQUEST';
export const CHANGE_CATEGORY = 'CHANGE_CATEGORY';
export const ADD_DISABLED_CATEGORY = 'ADD_DISABLED_CATEGORY';
export const REMOVE_DISABLED_CATEGORY = 'REMOVE_DISABLED_CATEGORY';

export const addCategories = (categories) => (dispatch) => {
  return dispatch({ type: ADD_CATEGORIES, payload: categories});
};

export const changeCategory = (category) => (dispatch) => {
  return dispatch({ type: CHANGE_CATEGORY, payload: category});
};

export const addDisabledCategory = (category) => (dispatch) => {
  return dispatch({ type: ADD_DISABLED_CATEGORY, payload: category});
};

export const removeDisabledCategory = (category) => (dispatch) => {
  return dispatch({ type: REMOVE_DISABLED_CATEGORY, payload: category});
};
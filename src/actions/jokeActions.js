export const ADD_JOKE = 'ADD_JOKE';
export const EDIT_JOKE = 'EDIT_JOKE';
export const REMOVE_JOKE = 'REMOVE_JOKE';
export const CHANGE_JOKE_FAVOURITE = 'CHANGE_JOKE_FAVOURITE';

export const addJoke = (joke) => (dispatch) => dispatch({ type: ADD_JOKE, payload: joke });

// eslint-disable-next-line camelcase
export const editJoke = (id, text, updated_at) => (dispatch) => dispatch({ type: EDIT_JOKE, payload: { id, text, updated_at } });

export const removeJoke = (id) => (dispatch) => dispatch({ type: REMOVE_JOKE, payload: id });

export const toggleJokeFavourite = (id) => (dispatch) => dispatch({ type: CHANGE_JOKE_FAVOURITE, payload: id });
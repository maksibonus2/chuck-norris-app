import thunk from "redux-thunk";
import configureMockStore from 'redux-mock-store';
import * as actions from "../categoryActions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Category actions', () => {
  const initialState = {};

  it('Should create an action to add categories', () => {
    const categories = ['TEST-CATEGORY1', 'TEST-CATEGORY2'];
    const expectedAction = [{
      type: actions.ADD_CATEGORIES,
      payload: categories
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.addCategories(categories));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  });

  it('Should create an action to change current category', () => {
    const category = 'TEST-CHANGED-CATEGORY';
    const expectedAction = [{
      type: actions.CHANGE_CATEGORY,
      payload: category
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.changeCategory(category));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  })
});
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import * as actions from "../jokeActions";
import type { Joke } from "../../types/jokeTypes";

const testJoke: Joke = {
  id: "TEST-ID",
  categories: [],
  created_at: "2020-01-01 13:42:21.179347",
  updated_at: "2020-01-01 13:42:21.179347",
  url: "",
  value: "TEST-JOKE",
  icon_url: "",
  isFavourite: false
};

const testChangedValue = "TEST-UPDATED-JOKE";
const testChangedTime = "2020-01-12 13:42:21.179347";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Joke actions', () => {
  const initialState = {};

  it('Should create an action to add joke', () => {
    const joke = testJoke;
    const expectedAction = [{
      type: actions.ADD_JOKE,
      payload: joke
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.addJoke(joke));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  });

  it('Should create an action to update joke', () => {
    const expectedAction = [{
      type: actions.EDIT_JOKE,
      payload: { id: testJoke.id, text: testChangedValue, updated_at: testChangedTime }
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.editJoke(testJoke.id, testChangedValue, testChangedTime));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  });

  it('Should create an action to remove joke', () => {
    const expectedAction = [{
      type: actions.REMOVE_JOKE,
      payload: testJoke.id
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.removeJoke(testJoke.id));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  });

  it('Should create an action to toggle joke as favourite', () => {
    const expectedAction = [{
      type: actions.CHANGE_JOKE_FAVOURITE,
      payload: testJoke.id
    }];

    const store = mockStore(initialState);
    store.dispatch(actions.toggleJokeFavourite(testJoke.id));

    const currentActions = store.getActions();

    expect(currentActions).toEqual(expectedAction);
  });
});
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './components/App/App';
import MaterialTheme from "./util/MaterialTheme";

const initialState = {};
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store}>
    <MaterialTheme>
      <App />
    </MaterialTheme>
  </Provider>,
  document.getElementById('root')
);
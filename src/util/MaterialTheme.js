import React from 'react';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const globalStyles = createMuiTheme({
  desktopAppBar: {
    marginBottom: "20px",
    backgroundColor: "#29b6f6"
  },
  mobileAppBar: {
    marginBottom: "10px",
    backgroundColor: "#29b6f6"
  },
  mobileTabs: {
    minHeight: "30px"
  },
  desktopTab: {
    fontSize: "25px",
    minHeight: 75,
    background: 'linear-gradient(to top, #30cfd0 0%, #330867 100%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    padding: '0 30px'
  },
  mobileTab: {
    fontSize: "15px",
    minHeight: 30,
    background: 'linear-gradient(to top, #30cfd0 0%, #330867 100%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    padding: '0 30px'
  },
  secondaryTabDesktop: {
    background: 'linear-gradient(to top, #30cfd0 0%, #330867 100%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    padding: '0 30px'
  },
  secondaryTabMobile: {
    fontSize: "10px",
    maxHeight: "20px",
    background: 'linear-gradient(to top, #30cfd0 0%, #330867 100%)',
    border: 0,
    borderRadius: 1,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white'
  },
  button: {
    backgroundColor: "#29b6f6"
  }
});

type Props = {
  children: React.ReactNode
}

const MaterialTheme = (props: Props) => (
  <MuiThemeProvider theme={globalStyles}>
    {props.children}
  </MuiThemeProvider>
);

export default MaterialTheme;
/* eslint-disable import/prefer-default-export,quote-props,consistent-return */

import axios from 'axios';
import conf from '../../conf/conf';

const JSON_HEADERS = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
};

function communicate(params) {
  const url = params.url;
  const method = params.method ? params.method.toLowerCase() : 'get';

  if (method === 'get') {
    return axios({
      method,
      url: conf.requestUrl + url,
      JSON_HEADERS
    });
  }

  return Promise.resolve();
}

export default communicate;

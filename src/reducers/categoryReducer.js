import * as actionType from "../actions/categoryActions";

export type State = {
  isInitialized: boolean,
  allCategories: Array<string>,
  disabledCategories: Array<string>,
  currentCategory: string
};

export const defaultState : State = {
  isInitialized: false,
  allCategories: ["all"],
  disabledCategories: [],
  currentCategory: "all"
};

const categoryReducer = (state: State = defaultState, action) => {
  switch (action.type) {
    case actionType.ADD_CATEGORIES:
      return { ...state, allCategories: [...state.allCategories, ...action.payload], isInitialized: true };
    case actionType.CHANGE_CATEGORY:
      return { ...state, currentCategory: action.payload };
    case actionType.ADD_DISABLED_CATEGORY:
      return { ...state, disabledCategories: [...state.disabledCategories, action.payload] };
    case actionType.REMOVE_DISABLED_CATEGORY: {
      const index = state.disabledCategories.findIndex((category) => category === action.payload);
      return { ...state, disabledCategories: [...state.disabledCategories.slice(0, index), ...state.disabledCategories.slice(index + 1)] };
    }
    default:
      return state;
  }
};

export default categoryReducer;
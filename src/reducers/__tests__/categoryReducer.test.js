import * as actions from "../../actions/categoryActions";
import reducer, { defaultState } from '../categoryReducer'

describe('Category reducer tests', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  it('Should handle ADD_CATEGORY', () => {
    const actionAdd = {
      type: actions.ADD_CATEGORIES,
      payload: ["TEST-CATEGORY"]
    };

    expect(
      reducer(undefined, actionAdd)
    ).toEqual({
      allCategories: ["all", "TEST-CATEGORY"],
      currentCategory: "all",
      disabledCategories: [],
      isInitialized: true
    });

    const actionAdd2 = {
      type: actions.ADD_CATEGORIES,
      payload: ["TEST-CATEGORY-2"]
    };

    expect(
      reducer({
        allCategories: ["all", "TEST-CATEGORY"],
        currentCategory: "all",
        disabledCategories: [],
        isInitialized: false
      }, actionAdd2)
    ).toEqual({
      allCategories: ["all", "TEST-CATEGORY", "TEST-CATEGORY-2"],
      currentCategory: "all",
      disabledCategories: [],
      isInitialized: true
    });
  })

  it('Should handle CHANGE_CATEGORY', () => {
    const actionEdit = {
      type: actions.CHANGE_CATEGORY,
      payload: "TEST-CATEGORY"
    };

    expect(
      reducer({
        allCategories: ["all", "TEST-CATEGORY"],
        currentCategory: "all"
      }, actionEdit)
    ).toEqual({
      allCategories: ["all", "TEST-CATEGORY"],
      currentCategory: "TEST-CATEGORY"
    });
  });
})
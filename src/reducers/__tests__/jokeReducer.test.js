import moment from "moment";
import * as actions from "../../actions/jokeActions";
import reducer, { defaultState } from '../jokeReducer'
import type { Joke } from "../../types/jokeTypes";

const testJoke: Joke = {
  id: "TEST-ID",
  categories: [],
  created_at: "2020-01-01 13:42:21.179347",
  updated_at: "2020-01-01 13:42:21.179347",
  url: "",
  value: "TEST-JOKE",
  icon_url: "",
  isFavourite: false
};

const testJoke2: Joke = {
  id: "TEST-ID2",
  categories: ["animal"],
  created_at: "2020-05-01 13:42:21.179347",
  updated_at: "2020-05-01 13:42:21.179347",
  url: "",
  value: "TEST-JOKE",
  icon_url: "",
  isFavourite: false
};

describe('Joke reducer tests', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  it('Should handle ADD_JOKE', () => {
    const actionAdd = {
      type: actions.ADD_JOKE,
      payload: testJoke
    };

    expect(
      reducer(undefined, actionAdd)
    ).toEqual({
      jokes: [testJoke]
    })

    expect(
      reducer(
        {
          jokes: [testJoke2]
        }, actionAdd
      )
    ).toEqual({
      jokes: [testJoke, testJoke2]
    })
  });

  it('Should handle EDIT_JOKE', () => {
    const updatedText = "TEST-UPDATED-JOKE";
    const currentTime = moment().format('YYYY-MM-DD HH:MM:ss.SSSSSS');

    const updatedJoke: Joke = {
      id: "TEST-ID",
      categories: [],
      created_at: "2020-01-01 13:42:21.179347",
      updated_at: currentTime,
      url: "",
      value: updatedText,
      icon_url: "",
      isFavourite: false
    };

    const actionEdit = {
      type: actions.EDIT_JOKE,
      payload: { id: "TEST-ID", text: updatedText, updated_at: currentTime }
    };

    expect(
      reducer({
        jokes: [testJoke]
      }, actionEdit)
    ).toEqual({
      jokes: [updatedJoke]
    });
  });

  it('Should handle REMOVE_JOKE', () => {
    const actionRemove = {
      type: actions.REMOVE_JOKE,
      payload: testJoke.id
    };

    expect(
      reducer({
        jokes: [testJoke]
      }, actionRemove)
    ).toEqual({
      jokes: []
    });
  });

  it('Should handle CHANGE_JOKE_FAVOURITE', () => {
    const updatedJoke: Joke = {
      id: "TEST-ID",
      categories: [],
      created_at: "2020-01-01 13:42:21.179347",
      updated_at: "2020-01-01 13:42:21.179347",
      url: "",
      value: "TEST-JOKE",
      icon_url: "",
      isFavourite: true
    };

    const actionChange = {
      type: actions.CHANGE_JOKE_FAVOURITE,
      payload: testJoke.id
    };

    expect(
      reducer({
        jokes: [testJoke]
      }, actionChange)
    ).toEqual({
      jokes: [updatedJoke]
    });
  });
})
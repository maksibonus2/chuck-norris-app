import moment from 'moment';
import * as actionType from "../actions/jokeActions";
import type { Joke } from "../types/jokeTypes";

export type State = {
  jokes: Array<Joke>
};

export const defaultState : State = {
  jokes: []
}

const sortJokes = (jokes: Array<Joke>) => {
  return jokes.sort((a, b) => moment(a.created_at) - moment(b.created_at));
}

const jokeReducer = (state: State = defaultState, action) => {
  switch (action.type) {
    case actionType.ADD_JOKE: {
      return {
        ...state,
        jokes: sortJokes([...state.jokes, action.payload])
      };
    }
    case actionType.EDIT_JOKE: {
      const index = state.jokes.findIndex((joke) => joke.id === action.payload.id);

      const joke = { ...state.jokes[index] };

      joke.value = action.payload.text;
      joke.updated_at = action.payload.updated_at;

      // delete old version of joke
      state.jokes.splice(index, 1);

      return {
        ...state,
        jokes: sortJokes([...state.jokes, joke])
      };
    }
    case actionType.REMOVE_JOKE: {
      const index = state.jokes.findIndex((joke) => joke.id === action.payload);
      return { ...state, jokes: [...state.jokes.slice(0, index), ...state.jokes.slice(index + 1)] };
    }
    case actionType.CHANGE_JOKE_FAVOURITE:
      return {
        ...state,
        jokes: state.jokes.map((x) => {
          if (x.id === action.payload) {
            return {
              ...x,
              isFavourite: !x.isFavourite
            }
          }
          return x;
        })
      };
    default:
      return state;
  }
};

export default jokeReducer;
// @flow
import { combineReducers } from "redux";
import jokeReducer from './jokeReducer';
import categoryReducer from "./categoryReducer";

export default combineReducers({
  jokeReducer,
  categoryReducer
});
const HtmlWebpackPlugin = require('html-webpack-plugin');

const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'development',
  devtool: 'eval',
  entry: {
    main: './src/index.js',
    bundle: ['react-hot-loader/patch', './src/index.js']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './src',
    historyApiFallback: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Chuck Norris APP',
      template: './src/index.template.ejs',
      inject: 'body'
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        include: path.join(__dirname, 'src')
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { sourcemap: true }
          }
        ]
      },
      {
        test: /\.(png|jpg|svg|gif)$/,
        use: ['file-loader']
      },
      { test: /\.(woff|woff2|eot|ttf)$/, loader: 'url-loader?limit=200000' }]
  },
  resolve: {
    extensions: ['.react.js', '.js', '.jsx']
  }
};

module.exports = {
  presets: [
    "@babel/preset-react",
    "@babel/preset-env",
    "@babel/preset-flow"
  ],
  plugins: [
    "react-hot-loader/babel",
    "transform-decorators-legacy",
    "@babel/plugin-proposal-function-sent",
    "@babel/plugin-proposal-export-namespace-from",
    "@babel/plugin-proposal-numeric-separator",
    "@babel/plugin-proposal-throw-expressions",
    "transform-inline-environment-variables"
  ],
  env: {
    development: {
      presets: [],
      plugins: [
        ["@babel/plugin-proposal-class-properties", { loose: true }],
        "@babel/plugin-proposal-object-rest-spread",
        "@babel/plugin-proposal-decorators",
        "babel-plugin-styled-components"]
    },
    production: {
      plugins: []
    },
    test: {
      plugins: [["@babel/plugin-proposal-decorators", { decoratorsBeforeExport: true }], "remove-decorator"]
    }
  }
}